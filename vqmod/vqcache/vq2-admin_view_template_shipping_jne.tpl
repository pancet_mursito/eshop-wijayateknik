<?php echo $header; ?> 
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      
			<h1><img src="view/image/admin_theme/base5builder_impulsepro/icon-shipping-large.png" alt="" /> <?php echo $heading_title; ?></h1>
			
      <div class="buttons">
      	<a onclick="$('#form').submit();" class="button"><span><?php echo $button_save; ?></span></a>
      	<a onclick="location = '<?php echo $cancel; ?>';" class="button"><span><?php echo $button_cancel; ?></span></a>
      </div>
    </div>
    <div class="content">
      <fieldset><legend>DATA SEKUNDER <span class="buttons" style="float:right"> <a href="#" class="button">Tambah</a></span></legend>
      <table class="list">
        <thead>
          <tr>
            <td class=" left">Kode</td>
            <td class=" left">Kota Tujuan</td>
            <td class="right">Ongkos</td>
            <td class="right">Tanggal Ubah</td>
          </tr>
        </thead>
        <tbody>
          <tr>
          	<td class=" left">1554</td>
            <td class=" left">Aceh Barat</td>
            <td class="right">78000</td>
            <td class="right">[dd/mm/yyyy]</td>
          </tr>
          
          <!--
          <?php if($extensions){ ?>
          <?php foreach ($extensions as $extension) { ?>
          <tr>
            <td class="left"><?php echo $extension['name']; ?></td>
            <td class="left"><?php echo $extension['status'] ?></td>
            <td class="right"><?php echo $extension['sort_order']; ?></td>
            <td class="right"><?php foreach ($extension['action'] as $action) { ?>
              [ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
              <?php } ?></td>
          </tr>
          <?php } ?>
          <?php } else { ?>
          <tr>
            <td class="center" colspan="8"><?php echo $text_no_results; ?></td>
          </tr>
          <?php } ?>
          -->
          
        </tbody>
      </table>
      </fieldset>
      <br/>
      <fieldset><legend>DATA PRIMER</legend>	
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
      <table class="form">
      	  <!--
          <tr>
          	<td><?= $entry_cost_id?></td>
          	<td><input type="text" name="__jne_cost_id" value="" size="10" required="required"/></td>
          </tr>
          <tr>
          	<td><?= $entry_cost_city?></td>
          	<td><input type="text" name="__jne_cost_city" value="" size="35" required="required"/></td>
          </tr>
          <tr>
          	<td><?= $entry_cost_price?></td>
          	<td><input type="number" name="__jne_cost_price" value="" size="35" required="required"/></td>
          </tr>
          -->
          <!--
          <tr>
          	<td><?= $entry_cost_weight?></td>
          	<td><input type="text" name="__jne_cost_weight" value="" size="5" /></td>
          </tr>
          --
          
          <tr>
            <td><?php echo $entry_cost; ?></td>
            <td><textarea name="jne_cost" cols="40" rows="5"><?php echo $jne_cost; ?></textarea></td>
          </tr>
          
          <!--
          <tr>
            <td><?php echo $entry_tax; ?></td>
            <td><select name="jne_tax_class_id">
                <option value="0"><?php echo $text_none; ?></option>
                <?php foreach ($tax_classes as $tax_class) { ?>
                <?php if ($tax_class['tax_class_id'] == $jne_tax_class_id) { ?>
                <option value="<?php echo $tax_class['tax_class_id']; ?>" selected="selected"><?php echo $tax_class['title']; ?></option>
                <?php } else { ?>
                <option value="<?php echo $tax_class['tax_class_id']; ?>"><?php echo $tax_class['title']; ?></option>
                <?php } ?>
                <?php } ?>
            </select></td>
          </tr>
          <tr>
            <td><?php echo $entry_geo_zone; ?></td>
            <td><select name="jne_geo_zone_id">
                <option value="0"><?php echo $text_all_zones; ?></option>
                <?php foreach ($geo_zones as $geo_zone) { ?>
                <?php if ($geo_zone['geo_zone_id'] == $jne_geo_zone_id) { ?>
                <option value="<?php echo $geo_zone['geo_zone_id']; ?>" selected="selected"><?php echo $geo_zone['name']; ?></option>
                <?php } else { ?>
                <option value="<?php echo $geo_zone['geo_zone_id']; ?>"><?php echo $geo_zone['name']; ?></option>
                <?php } ?>
                <?php } ?>
              </select></td>
          </tr>
          -->
          
          <tr>
            <td><?php echo $entry_status; ?></td>
            <td><select name="jne_status">
                <?php if ($jne_status) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select></td>
          </tr>
          <tr>
            <td><?php echo $entry_sort_order; ?></td>
            <td><input type="text" name="jne_sort_order" value="<?php echo $jne_sort_order; ?>" size="1" /></td>
          </tr>
		  <tr>
            <td>Pengembang</td>
            <td><a href="http://www.pijaronline.com" target="_blank">www.PijarOnline.com</a> 
            	custom by <a href="http://www.greenhole.web.id" target="_blank">www.greenhole.web.id</a>
            </td>
          </tr>
      </table>
      </form>
      </fieldset>
    </div>
  </div>
</div>
<?php echo $footer; ?> 