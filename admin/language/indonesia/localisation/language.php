<?php
// Heading
$_['heading_title']     = 'Bahasa';  

// Text
$_['text_success']      = 'Sukses: Memoodifikasi bahasa!'; 

// Column
$_['column_name']       = 'Nama Bahasa';
$_['column_code']       = 'Kode';
$_['column_sort_order'] = 'Urutan';
$_['column_action']     = 'Tindakan';

// Entry
$_['entry_name']        = 'Nama Bahasa:';
$_['entry_code']        = 'Kode:<br /><span class="help">eg: en. Do not change if this is your default language.</span>';
$_['entry_locale']      = 'Lokal:<br /><span class="help">eg: en_US.UTF-8,en_US,en-gb,en_gb,english</span>';
$_['entry_image']       = 'Gambar:<br /><span class="help">eg: gb.png</span>';
$_['entry_directory']   = 'Direktori:<br /><span class="help">name of the language directory (case-sensitive)</span>';
$_['entry_filename']    = 'Nama File:<br /><span class="help">main language filename without extension</span>';
$_['entry_status']      = 'Status:<br /><span class="help">Hide/Show it in language dropdown</span>';
$_['entry_sort_order']  = 'Urutan:';

// Error
$_['error_permission']  = 'Warning: You do not have permission to modify languages!';
$_['error_name']        = 'Language Name must be between 3 and 32 characters!';
$_['error_code']        = 'Language Code must at least 2 characters!';
$_['error_locale']      = 'Locale required!';
$_['error_image']       = 'Image Filename must be between 3 and 64 characters!';
$_['error_directory']   = 'Directory required!';
$_['error_filename']    = 'Filename must be between 3 and 64 characters!';
$_['error_default']     = 'Warning: This language cannot be deleted as it is currently assigned as the default store language!';
$_['error_admin']       = 'Warning: This Language cannot be deleted as it is currently assigned as the administration language!';
$_['error_store']       = 'Warning: This language cannot be deleted as it is currently assigned to %s stores!';
$_['error_order']       = 'Warning: This language cannot be deleted as it is currently assigned to %s orders!';
?>