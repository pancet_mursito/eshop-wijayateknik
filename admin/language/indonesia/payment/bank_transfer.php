<?php
// Heading
$_['heading_title']      = 'Transfer Bank';

// Text 
$_['text_payment']       = 'Pembayaran';
$_['text_success']       = 'Sukses: Anda telah memodifikasi rincian transfer bank!';

// Entry
$_['entry_bank']         = 'Instruksi Transfer Bank:';
$_['entry_total']        = 'Total:<br /><span class="help">Total order checkout harus terpenuhi sebelum metode pembayaran ini menjadi aktif.</span>';
$_['entry_order_status'] = 'Status Order:';
$_['entry_geo_zone']     = 'Zona Geo:';
$_['entry_status']       = 'Status:';
$_['entry_sort_order']   = 'Urutan:';

// Error
$_['error_permission']   = 'Peringatan: Anda tidak memiliki izin untuk mengubah pembayaran transfer bank!';
$_['error_bank']         = 'Instruksi Transfer Bank, diperlukan!';
$_['error_required']     = 'Harus diisi!';
?>