<?php
// Heading
$_['heading_title']          = 'Produk'; 

// Text  
$_['text_success']           = 'Sukses: Anda telah memodifikasi pengaturan produk!';
$_['text_plus']              = '+';
$_['text_minus']             = '-';
$_['text_default']           = 'Standar';
$_['text_image_manager']     = 'Pengatur Gambar';
$_['text_browse']            = 'Jelajah';
$_['text_clear']             = 'Clear';
$_['text_option']            = 'Opsi';
$_['text_option_value']      = 'Opsi Nilai';
$_['text_percent']           = 'Prosentase';
$_['text_amount']            = 'Fixed Amount';

// Column
$_['column_name']            = 'Nama Produk';
$_['column_model']           = 'Model';
$_['column_image']           = 'Gambar';
$_['column_price']           = 'Harga';
$_['column_quantity']        = 'Kuantitas';
$_['column_status']          = 'Status';
$_['column_action']          = 'Tindakan';

// Entry
$_['entry_name']             = 'Nama Produk:';
$_['entry_meta_keyword']     = 'Meta Tag Keywords:';
$_['entry_meta_description'] = 'Meta Tag Description:';
$_['entry_description']      = 'Deskripsi:';
$_['entry_store']            = 'Toko:';
$_['entry_keyword']          = 'SEO Keyword:<br /><span class="help">Do not use spaces instead replace spaces with - and make sure the keyword is globally unique.</span>';
$_['entry_model']            = 'Model:';
$_['entry_sku']              = 'SKU:<br/><span class="help">Stock Keeping Unit</span>';
$_['entry_upc']              = 'UPC:<br/><span class="help">Universal Product Code</span>';
$_['entry_ean']              = 'EAN:<br/><span class="help">European Article Number</span>';
$_['entry_jan']              = 'JAN:<br/><span class="help">Japanese Article Number</span>';
$_['entry_isbn']             = 'ISBN:<br/><span class="help">International Standard Book Number</span>';
$_['entry_mpn']              = 'MPN:<br/><span class="help">Manufacturer Part Number</span>';
$_['entry_location']         = 'Lokasi:';
$_['entry_shipping']         = 'Requires Shipping:'; 
$_['entry_manufacturer']     = 'Manufacturer:<br /><span class="help">(Autocomplete)</span>';
$_['entry_date_available']   = 'Tanggal Tersedia:';
$_['entry_quantity']         = 'Kuantitas:';
$_['entry_minimum']          = 'Kuantitas Minimal:<br/><span class="help">Force a minimum ordered amount</span>';
$_['entry_stock_status']     = 'Status Stok Habis:<br/><span class="help">Status shown when a product is out of stock</span>';
$_['entry_price']            = 'Harga:';
$_['entry_tax_class']        = 'Satuan Pajak:';
$_['entry_points']           = 'Poin:<br/><span class="help">Number of points needed to buy this item. If you don\'t want this product to be purchased with points leave as 0.</span>';
$_['entry_option_points']    = 'Poin:';
$_['entry_subtract']         = 'Subtract Stock:';
$_['entry_weight_class']     = 'Satuan Berat:';
$_['entry_weight']           = 'Berat:';
$_['entry_length']           = 'Satuan Panjang:';
$_['entry_dimension']        = 'Dimensi (L x W x H):';
$_['entry_image']            = 'Gambar:';
$_['entry_customer_group']   = 'Grup Pelanggan:';
$_['entry_date_start']       = 'Tanggal Mulai:';
$_['entry_date_end']         = 'Tanggal Selesai:';
$_['entry_priority']         = 'Prioritas:';
$_['entry_attribute']        = 'Atribut:';
$_['entry_attribute_group']  = 'Grup Atribut:';
$_['entry_text']             = 'Teks:';
$_['entry_option']           = 'Opsi:';
$_['entry_option_value']     = 'Opsi Nilai:';
$_['entry_required']         = 'Diisi:';
$_['entry_status']           = 'Status:';
$_['entry_sort_order']       = 'Urutan:';
$_['entry_category']         = 'Kategori:<br /><span class="help">(Autocomplete)</span>';
$_['entry_filter']           = 'Filter:<br /><span class="help">(Autocomplete)</span>';
$_['entry_download']         = 'Unduhan:<br /><span class="help">(Autocomplete)</span>';
$_['entry_related']          = 'Related Products:<br /><span class="help">(Autocomplete)</span>';
$_['entry_tag']              = 'Product Tags:<br /><span class="help">comma separated</span>';
$_['entry_reward']           = 'Poin Reward:';
$_['entry_layout']           = 'Layout Override:';

// Error
$_['error_warning']          = 'Warning: Please check the form carefully for errors!';
$_['error_permission']       = 'Warning: You do not have permission to modify products!';
$_['error_name']             = 'Product Name must be greater than 3 and less than 255 characters!';
$_['error_model']            = 'Product Model must be greater than 3 and less than 64 characters!';
?>