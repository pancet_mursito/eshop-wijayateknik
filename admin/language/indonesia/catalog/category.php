<?php
// Heading
$_['heading_title']          = 'Kategori';

// Text
$_['text_success']           = 'Success: You have modified categories!';
$_['text_default']           = 'Standar';
$_['text_image_manager']     = 'Manager Gambar';
$_['text_browse']            = 'Jelajah';
$_['text_clear']             = 'Clear';

// Column
$_['column_name']            = 'Nama Kategori';
$_['column_sort_order']      = 'Urutan';
$_['column_action']          = 'Aksi';
$_['column_status']          = 'Status';


// Entry
$_['entry_name']             = 'Nama Kategori:';
$_['entry_meta_keyword']     = 'Meta Tag Kata Kunci:';
$_['entry_meta_description'] = 'Meta Tag Deskripsi:';
$_['entry_description']      = 'Deskripsi:';
$_['entry_parent']           = 'Induk:';
$_['entry_filter']           = 'Filters:<br /><span class="help">(Autocomplete)</span>';
$_['entry_store']            = 'Stores:';
$_['entry_keyword']          = 'SEO Keyword:<br /><span class="help">Do not use spaces instead replace spaces with - and make sure the keyword is globally unique.</span>';
$_['entry_image']            = 'Gambar:';
$_['entry_top']              = 'Top:<br/><span class="help">Display in the top menu bar. Only works for the top parent categories.</span>';
$_['entry_column']           = 'Columns:<br/><span class="help">Number of columns to use for the bottom 3 categories. Only works for the top parent categories.</span>';
$_['entry_sort_order']       = 'Urutan:';
$_['entry_status']           = 'Status:';
$_['entry_layout']           = 'Layout Override:';

// Error 
$_['error_warning']          = 'Warning: Please check the form carefully for errors!';
$_['error_permission']       = 'Warning: You do not have permission to modify categories!';
$_['error_name']             = 'Category Name must be between 2 and 32 characters!';
?>