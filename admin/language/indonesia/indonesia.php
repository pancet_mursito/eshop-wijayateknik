<?php
// Locale
$_['code']                          = 'id';
$_['direction']                     = 'ltr';
$_['date_format_short']             = 'd/m/Y';
$_['date_format_long']              = 'l dS F Y';
$_['time_format']                   = 'h:i:s A';
$_['decimal_point']                 = '.';
$_['thousand_point']                = ',';

// Text
$_['text_yes']                      = 'Ya';
$_['text_no']                       = 'Tidak';
$_['text_enabled']                  = 'Diaktifkan';
$_['text_disabled']                 = 'Dinonaktifkan';
$_['text_none']                     = ' --- Tak Satupun --- ';
$_['text_select']                   = ' --- Silakan Pilih --- ';
$_['text_select_all']               = 'Pilih Semua';
$_['text_unselect_all']             = 'Jangan Pilih Semua';
$_['text_all_zones']                = 'Semua Zona';
$_['text_default']                  = ' <b>(Default)</b>';
$_['text_close']                    = 'Tutup';
$_['text_pagination']               = 'Menampilkan {start} ke {end} dari {total} ({pages} Halaman)';
$_['text_no_results']               = 'Tidak Ada Hasil!';
$_['text_separator']                = ' &gt; ';
$_['text_edit']                     = 'Ubah';
$_['text_view']                     = 'Lihat';
$_['text_home']                     = 'Beranda';

// Button
$_['button_insert']                 = 'Sisipkan';
$_['button_delete']                 = 'Hapus';
$_['button_save']                   = 'Simpan';
$_['button_cancel']                 = 'Batal';
$_['button_clear']                  = 'Hapus Semua Log';
$_['button_close']                  = 'Tutup';
$_['button_filter']                 = 'Saring';
$_['button_send']                   = 'Kirim';
$_['button_edit']                   = 'Ubah';
$_['button_copy']                   = 'Salin';
$_['button_back']                   = 'Kembali';
$_['button_remove']                 = 'Menghapus';
$_['button_backup']                 = 'Backup';
$_['button_restore']                = 'Pulihkan';
$_['button_repair']                 = 'Memperbaiki';
$_['button_upload']                 = 'Unggah';
$_['button_submit']                 = 'Serahkan';
$_['button_invoice']                = 'Cetak Tagihan';
$_['button_add_address']            = 'Tambah Alamat';
$_['button_add_attribute']          = 'Tambah Atribut';
$_['button_add_banner']             = 'Tambah Banner';
$_['button_add_custom_field_value'] = 'Tambah Bidang Khusus';
$_['button_add_product']            = 'Tambah Produk';
$_['button_add_voucher']            = 'Tambah Voucher';
$_['button_add_filter']             = 'Tambah Saringan';
$_['button_add_option']             = 'Tambah Opsi';
$_['button_add_option_value']       = 'Tambah Nilai Opsi';
$_['button_add_discount']           = 'Tambah Diskon';
$_['button_add_special']            = 'Tambah Spesial';
$_['button_add_image']              = 'Tambah Gambar';
$_['button_add_geo_zone']           = 'Tambah Zona Waktu';
$_['button_add_history']            = 'Tambah Riwayat';
$_['button_add_transaction']        = 'Tambah Transaksi';
$_['button_add_total']              = 'Tambah Total';
$_['button_add_reward']             = 'Tambah Poin Hadiah';
$_['button_add_route']              = 'Tambah Rute';
$_['button_add_rule' ]              = 'Tambah Aturan';
$_['button_add_module']             = 'Tambah Modul';
$_['button_add_link']               = 'Tambah Link';
$_['button_update_total']           = 'Perbarui Total';
$_['button_approve']                = 'Setuju';
$_['button_reset']                  = 'Ulangi';

// Tab
$_['tab_address']                   = 'Alamat';
$_['tab_admin']                     = 'Admin';
$_['tab_attribute']                 = 'Atribut';
$_['tab_customer']                  = 'Rincian Konsumen';
$_['tab_data']                      = 'Data';
$_['tab_design']                    = 'Desain';
$_['tab_discount']                  = 'Diskon';
$_['tab_general']                   = 'Umum';
$_['tab_history']                   = 'Riwayat';
$_['tab_fraud']                     = 'Penipuan';
$_['tab_ftp']                       = 'FTP';
$_['tab_ip']                        = 'Alamat IP';
$_['tab_links']                     = 'Link';
$_['tab_log']                       = 'Log';
$_['tab_image']                     = 'Gambar';
$_['tab_option']                    = 'Opsi';
$_['tab_server']                    = 'Server';
$_['tab_store']                     = 'Toko/Site';
$_['tab_special']                   = 'Spesial';
$_['tab_local']                     = 'Lokal';
$_['tab_mail']                      = 'Surel';
$_['tab_module']                    = 'Modul';
$_['tab_order']                     = 'Rincian Pesanan';
$_['tab_payment']                   = 'Rincian Pembayaran';
$_['tab_product']                   = 'Produk';
$_['tab_return']                    = 'Rincian Kembali';
$_['tab_reward']                    = 'Poin Hadiah';
$_['tab_shipping']                  = 'Rincian Pengiriman';
$_['tab_total']                     = 'Total';
$_['tab_transaction']               = 'Transaksi';
$_['tab_voucher']                   = 'Voucher';
$_['tab_voucher_history']           = 'Riwayat Voucher';

// Error
$_['error_upload_1']                = 'Peringatan: Berkas yang diunggah melebihi direktif upload_max_filesize dalam php.ini!';
$_['error_upload_2']                = 'Peringatan: Berkas yang diunggah melampaui direktif MAX_FILE_SIZE yang ditentukan dalam bentuk HTML!';
$_['error_upload_3']                = 'Peringatan: Berkas yang diunggah hanya berhasil diunggah sebagian!';
$_['error_upload_4']                = 'Peringatan: Tidak ada file yang diupload!';
$_['error_upload_6']                = 'Peringatan: Folder sementara tidak ada!';
$_['error_upload_7']                = 'Peringatan: Gagal menulis file ke disk!';
$_['error_upload_8']                = 'Peringatan: File upload dihentikan oleh ekstensi!';
$_['error_upload_999']              = 'Peringatan: Tidak ada kesalaha kode tersedia!';
?>