<?php
// Heading
$_['heading_title']                 = 'Dashboard';

// Text
$_['text_overview']                 = 'Ringkasan';
$_['text_statistics']               = 'Statistik';
$_['text_latest_10_orders']         = '10 Order Terbaru';
$_['text_total_sale']               = 'Total Penjualan';
$_['text_total_sale_year']          = 'Total Penjualan Tahun Ini';
$_['text_total_sales_previous_years']='Total Penjualan Tahun Sebelumnya'; 
$_['text_total_review']             = 'Total Tinjauan'; 
$_['text_total_order']              = 'Total Order ';
$_['text_total_customer']           = 'No. Pelanggan';
$_['text_total_customer_approval']  = 'Pelanggan Menunggu Persetujuan';
$_['text_total_review_approval']    = 'Ulasan Menunggu Persetujuan';
$_['text_total_affiliate']          = 'No. Afiliasi';
$_['text_total_affiliate_approval'] = 'Afiliasi Menunggu Persetujuan';
$_['text_day']                      = 'Hari Ini';
$_['text_week']                     = 'Minggu Ini';
$_['text_month']                    = 'Bulan Ini';
$_['text_year']                     = 'Tahun Ini';
$_['text_order']                    = 'Total Order';
$_['text_customer']                 = 'Total Pelanggan';
$_['text_other_stats']              = 'Stat Lain';

// Column 
$_['column_order']                  = 'ID Order';
$_['column_customer']               = 'Pelanggan';
$_['column_status']                 = 'Status';
$_['column_date_added']             = 'Tanggal Ubah';
$_['column_total']                  = 'Total';
$_['column_firstname']              = 'Nama Depan';
$_['column_lastname']               = 'Nama Belakang';
$_['column_action']                 = 'Tindakan';

// Entry
$_['entry_range']                   = 'Pilih Periode:';

// Error
$_['error_install']                 = 'Peringatan: Install folder masih ada dan harus dihapus karena alasan keamanan!';
$_['error_image']                   = 'Peringatan: Direktori gambar %s tidak bisa ditulis!';
$_['error_image_cache']             = 'Peringatan: Direktori cache gambar %s tidak bisa ditulis!';
$_['error_cache']                   = 'Peringatan: Direktori cache %s tidak bisa ditulis!';
$_['error_download']                = 'Peringatan: Direktori unduhan %s tidak bisa ditulis!';
$_['error_logs']                    = 'Peringatan: Direktori log %s tidak bisa ditulis!';
?>