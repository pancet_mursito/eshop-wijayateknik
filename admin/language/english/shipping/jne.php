<?php
// Heading
$_['heading_title']         = 'JNE Indonesia Regular';

// Text
$_['text_shipping']         = 'Shipping';
$_['text_success']          = 'Success: You have modified Jne rate shipping!';
$_['text_no_results']       = 'No Results';
$_['text_select_province']  = 'Select Province';
$_['text_legend_primary']   = 'Primary Data';
$_['text_legend_secondary'] = 'Secondary Data';

// Button
#
$_['button_add_rates']      = 'Add';
$_['button_cancel_addrates']= 'Cancel';
$_['button_edit_rates']     = 'Edit'; 
$_['button_remove']         = 'Delete';
#
// Entry
#
$_['entry_cost_code']       = 'Province Code';
$_['entry_province']        = 'Province';
$_['entry_cost_city']       = 'Destination City';
$_['entry_cost_price']      = 'Price/Rates';
$_['entry_cost_weight']     = 'Weight (Kg)';
#
$_['entry_hometown']        = 'Town Early';
$_['entry_cost']            = 'Fare';
$_['entry_tax']             = 'Tax Class';
$_['entry_geo_zone']        = 'Geo Zone';
$_['entry_status']          = 'Status';
$_['entry_sort_order']      = 'Sort Order';

// Fieldname
#
#$_['col_rates_code']       = 'Code';
#$_['col_rates_city']       = 'Destination City';
#$_['col_rates_price']      = 'Rates';
$_['col_rates_date']        = 'Date Added';
$_['col_rates_act']         = 'Action';
#
// Error
$_['error_permission']      = 'Warning: You do not have permission to modify Jne rate shipping!';
?>