<?= $header; ?> 
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?= $breadcrumb['separator']; ?><a href="<?= $breadcrumb['href']; ?>"><?= $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?= $error_warning; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/shipping.png" alt="" /> <?= $heading_title; ?></h1>
      <div class="buttons">
      	<a onclick="$('#form').submit();" class="button"><span><?= $button_save; ?></span></a>
      	<a onclick="location = '<?= $cancel; ?>';" class="button"><span><?= $button_cancel; ?></span></a>
      	<a onclick="$('#form-table').submit();" id="btn-delete-rates" class="button delete"><?= $button_remove; ?></a>
      </div>
    </div>
    <div class="content">
      <!--
	  <div class="success" style="display:<?= !isset($success) ? 'none' : ''; ?>">
	  	<?= !isset($success) ? '' : $success; ?>
	  </div>
	  <div class="warning" style="display:<?= !isset($error) ? 'none' : ''; ?>">
	  	<?= !isset($error) ? '' : $error; ?>
	  </div>
      -->	
      <fieldset id="wrap-secondary">
      <legend><?= strtoupper($text_legend_secondary)?> 
       		  <span class="buttons" style="float:right"> 
       		    <a class="button add-rates"><?= $button_add_rates?></a>
       		  </span>
      </legend>
      <form id="form-table" method="post" action="<?= $delete?>">
      <table class="scroll">
        <thead>
          <tr>
            <td class="center tiny">
             <input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" />
            </td>
            <td class="center"><?= $entry_cost_code?></td>
            <td class="center large"><?= $entry_cost_city?></td>
            <td class="center"><?= $entry_cost_price?></td>
            <td class="center"><?= $col_rates_date?></td>
            <td class="center"><?= $col_rates_act?></td>
          </tr>
        </thead>
        <tbody>
          <?php if(count($tarif_ekspedisi) > 0){ ?>	
          <?php foreach ($tarif_ekspedisi as $ii => $data){ ?>	
          <tr>
            <td class="center tiny">
             <input type="checkbox" name="selected[]" value="<?= $data['province_code']?>|<?= $data['cityname']?>" />
            </td>
            <td class="center"><?= $data['province_code']?></td>
            <td class="left large">&nbsp;<?= $data['cityname']?></td>
            <td class="right "><?= $data['rates']?>&nbsp;</td>
            <td class="center"><?= $data['date_added']?></td>
            <td class="center">
             [ <a class="edit-rates" provcode="<?= $data['province_code']?>" cityname="<?= $data['cityname']?>" rates="<?= $data['rates']?>">
               <?= $button_edit_rates?>
               </a> ] &nbsp;
            </td>
          </tr>
          <?php } ?>
          <?php } else { ?>
          <tr><td class="center" colspan="6"><?= $text_no_results; ?></td></tr>
          <?php } ?>
        </tbody>
      </table>
      </form>
      <!--?= $pagination;?-->
      
      <form action="<?= $action; ?>" method="post" enctype="multipart/form-data" id="formx" class="f-secondary" style="display:none">
      <input type ="hidden" name="form_category" value="secondary" />
      <table class="form">
        <tr>
         <td><?= $entry_province?></td>
         <td server-lang="<?= $_SERVER['HTTP_COOKIE']?>">
         <select name="shipping_code">
           <option value="">--<?= $text_select_province?>--</option>
           <?php foreach($province_tags as $data){ ?>
           <option value="<?= $data['zone_id']?>"><?= $data['name']?></option>
           <?php } ?>
         </select>
         </td>
         </tr>
        <tr>
         <td><?= $entry_cost_city?></td>
         <td><input type="text" name="shipping_city" value="" size="40" required="required"/></td>
        </tr>
        <tr>
         <td><?= $entry_cost_price?></td>
         <td><input type="number" name="shipping_rate" value="" size="30" required="required"/></td>
        </tr>
        <tr>
         <td colspan="2" align="right">
         	<input type="button" class="cancel-add-rates" value="<<" title="<?= $button_cancel_addrates?>" />
         </td>
        </tr>
      </table>
      </form>
      </fieldset>
      <br/>
      
      <fieldset id="wrap-primary">
      <legend><?= strtoupper($text_legend_primary)?></legend>	
      <form action="<?= $action; ?>" method="post" enctype="multipart/form-data" id="form" class="f-primary">
      <input type ="hidden" name="form_category" value="primary" />
      <table class="form">
        <input type="hidden" name="jne_cost" value="" />
        <!--
        <tr>
         <td><?= $entry_cost; ?></td>
         <td><textarea name="jne_cost" cols="40" rows="5"><?= $jne_cost; ?></textarea></td>
        </tr>  
        --
        <tr>
         <td><?php echo $entry_tax; ?></td>
         <td><select name="jne_tax_class_id">
             <option value="0"><?= $text_none; ?></option>
             <?php foreach ($tax_classes as $tax_class) { ?>
             <?php if ($tax_class['tax_class_id'] == $jne_tax_class_id) { ?>
             <option value="<?= $tax_class['tax_class_id']; ?>" selected="selected"><?= $tax_class['title']; ?></option>
             <?php } else { ?>
             <option value="<?= $tax_class['tax_class_id']; ?>"><?= $tax_class['title']; ?></option>
             <?php } ?>
             <?php } ?>
         </select></td>
        </tr>
        -->
        <tr>
         <td><?php echo $entry_geo_zone; ?></td>
         <td><select name="jne_geo_zone_id">
             <option value="0"><?php echo $text_all_zones; ?></option>
             <?php foreach ($geo_zones as $geo_zone) { ?>
             <?php if ($geo_zone['geo_zone_id'] == $jne_geo_zone_id) { ?>
             <option value="<?php echo $geo_zone['geo_zone_id']; ?>" selected="selected"><?php echo $geo_zone['name']; ?></option>
             <?php } else { ?>
             <option value="<?php echo $geo_zone['geo_zone_id']; ?>"><?php echo $geo_zone['name']; ?></option>
             <?php } ?>
             <?php } ?>
         </select></td>
        </tr>
        <!-- 
        <tr>
         <td><?= $entry_hometown; ?></td>
         <td><input type="text" name="jne_hometown" value="<?= $jne_hometown; ?>" size="40" /></td>
        </tr>
        -->
        
        <tr>
         <td><?= $entry_status; ?></td>
         <td><select name="jne_status">
         	 <?php if ($jne_status) { ?>
         	 <option value="1" selected="selected"><?= $text_enabled; ?></option>
         	 <option value="0"><?= $text_disabled; ?></option>
         	 <?php } else { ?>
         	 <option value="1"><?= $text_enabled; ?></option>
         	 <option value="0" selected="selected"><?= $text_disabled; ?></option>
         	 <?php } ?>
         </select></td>
        </tr>
        <tr>
         <td><?= $entry_sort_order; ?></td>
         <td><input type="text" name="jne_sort_order" value="<?= $jne_sort_order; ?>" size="1" /></td>
        </tr>
		<tr>
         <td>Pengembang</td>
         <td><a href="http://www.pijaronline.com" target="_blank">www.PijarOnline.com</a> 
             &AMP; <a href="http://www.greenhole.web.id" target="_blank">www.greenhole.web.id</a>
         </td>
        </tr>
      </table>
      </form>
      </fieldset>
    </div>
  </div>
</div>

<script type="text/javascript">

$(function(){ // Open jQuery[[
	$('form.f-secondary').find('input.edit-added').remove();
	
	$('.add-rates').on('click', function(e){
		e.preventDefault();
		$('form.f-secondary').find('input[type=text], input[type=number], select, textarea').val('');
		$('form.f-secondary').find('input[name=form_category]').val('secondary');
		$('form.f-secondary').find('input.edit-added').remove();
		$('form.f-secondary').slideDown().attr('id','form');
		$('#wrap-primary').slideUp();
		$('form.f-primary').attr('id','formx');
	});	
	$('.cancel-add-rates').on('click', function(e){
		e.preventDefault();
		$('form.f-secondary').slideUp().attr('id','formx');
		$('form.f-secondary').find('input.edit-added').remove();
		$('#wrap-primary').slideDown();
		$('form.f-primary').attr('id','form');
	});	
	$('.edit-rates').on('click', function(e){
		e.preventDefault();
		var $$ = $(this); 
		$('form.f-secondary')
		.append('<input type="hidden" class="edit-added" name="old_shipping_code" value="'+ $$.attr('provcode') +'"/>')
		.append('<input type="hidden" class="edit-added" name="old_shipping_city" value="'+ $$.attr('cityname') +'"/>')
		.append('<input type="hidden" class="edit-added" name="old_shipping_rate" value="'+ $$.attr('rates') +'"/>');
		
		$('form.f-secondary').find('select[name=shipping_code]').val($$.attr('provcode'));
		$('form.f-secondary').find(' input[name=shipping_city]').val($$.attr('cityname'));
		$('form.f-secondary').find(' input[name=shipping_rate]').val($$.attr('rates'));
		$('form.f-secondary').find(' input[name=form_category]').val('secondary-update');
		$('form.f-secondary').attr('id','form').slideDown();
		$('#wrap-primary').slideUp();
		$('form.f-primary').attr('id','formx');
	});
}) // Close jQuery]]

</script>

<?= $footer; ?> 