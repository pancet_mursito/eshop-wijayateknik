
</div> <!-- /container -->
	<!-- Le javascript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script type="text/javascript" src="view/javascript/admin_theme/base5builder_impulsepro/bootstrap-transition-min.js"></script>
	<script type="text/javascript" src="view/javascript/admin_theme/base5builder_impulsepro/bootstrap-alert-min.js"></script>
	<script type="text/javascript" src="view/javascript/admin_theme/base5builder_impulsepro/bootstrap-modal-min.js"></script>
	<script type="text/javascript" src="view/javascript/admin_theme/base5builder_impulsepro/bootstrap-dropdown-min.js"></script>
	<script type="text/javascript" src="view/javascript/admin_theme/base5builder_impulsepro/bootstrap-scrollspy-min.js"></script>
	<script type="text/javascript" src="view/javascript/admin_theme/base5builder_impulsepro/bootstrap-tab-min.js"></script>
	<script type="text/javascript" src="view/javascript/admin_theme/base5builder_impulsepro/bootstrap-tooltip-min.js"></script>
	<script type="text/javascript" src="view/javascript/admin_theme/base5builder_impulsepro/bootstrap-popover-min.js"></script>
	<script type="text/javascript" src="view/javascript/admin_theme/base5builder_impulsepro/bootstrap-button-min.js"></script>
	<script type="text/javascript" src="view/javascript/admin_theme/base5builder_impulsepro/bootstrap-collapse-min.js"></script>
	<script type="text/javascript" src="view/javascript/admin_theme/base5builder_impulsepro/bootstrap-carousel-min.js"></script>
	<script type="text/javascript" src="view/javascript/admin_theme/base5builder_impulsepro/bootstrap-typeahead-min.js"></script>
	<script type="text/javascript" src="view/javascript/admin_theme/base5builder_impulsepro/scriptbreaker-multiple-accordion-1-min.js"></script>
	<?php if($this->user->getUserName()){ ?>
	<script type="text/javascript" src="view/javascript/admin_theme/base5builder_impulsepro/script.js"></script>
	<?php } ?>

</body>
</html>