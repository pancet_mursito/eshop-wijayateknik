<?= $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?= $breadcrumb['separator']; ?><a href="<?= $breadcrumb['href']; ?>"><?= $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?= $error_warning; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/payment.png" alt="" /> <?= $heading_title; ?></h1>
      <div class="buttons">
          <a onclick="$('#form').submit();" class="button"><?= $button_save; ?></a>
          <a href="<?= $cancel; ?>" class="button"><?= $button_cancel; ?></a>
      </div>
    </div>
    <div class="content">
      <div id="tabs" class="htabs">
          <a href="#tab-general"><?= $tab_general; ?></a>
          <!-- <a href="#tab-data"><?= $tab_data; ?></a> -->
      </div>  
      <form action="<?= $action; ?>" method="post" enctype="multipart/form-data" id="form" class="ftabs">
        <input type="hidden" name="form_category" value="fgeneral" />     
        <div id="tab-general"> 
        <table class="form">
          <?php foreach ($languages as $language) { ?>
          <tr>
            <td><span class="required">*</span> <?= $entry_bank; ?></td>
            <td><textarea name="bank_transfer_bank_<?php echo $language['language_id']; ?>" cols="80" rows="10"><?= isset(${'bank_transfer_bank_' . $language['language_id']}) ? ${'bank_transfer_bank_' . $language['language_id']} : ''; ?></textarea>
              <img src="view/image/flags/<?= $language['image']; ?>" title="<?= $language['name']; ?>" style="vertical-align: top;" /><br />
              <?php if (isset(${'error_bank_' . $language['language_id']})) { ?>
              <span class="error"><?= ${'error_bank_' . $language['language_id']}; ?></span>
              <?php } ?></td>
          </tr>
          <?php } ?>
          <tr>
            <td><?= $entry_total; ?></td>
            <td><input type="text" name="bank_transfer_total" value="<?= $bank_transfer_total; ?>" /></td>
          </tr>
          <tr>
            <td><?= $entry_order_status; ?></td>
            <td><select name="bank_transfer_order_status_id">
                <?php foreach ($order_statuses as $order_status) { ?>
                <?php if ($order_status['order_status_id'] == $bank_transfer_order_status_id) { ?>
                <option value="<?= $order_status['order_status_id']; ?>" selected="selected"><?= $order_status['name']; ?></option>
                <?php } else { ?>
                <option value="<?= $order_status['order_status_id']; ?>"><?= $order_status['name']; ?></option>
                <?php } ?>
                <?php } ?>
              </select></td>
          </tr>
          <tr>
            <td><?= $entry_geo_zone; ?></td>
            <td><select name="bank_transfer_geo_zone_id">
                <option value="0"><?= $text_all_zones; ?></option>
                <?php foreach ($geo_zones as $geo_zone) { ?>
                <?php if ($geo_zone['geo_zone_id'] == $bank_transfer_geo_zone_id) { ?>
                <option value="<?= $geo_zone['geo_zone_id']; ?>" selected="selected"><?= $geo_zone['name']; ?></option>
                <?php } else { ?>
                <option value="<?= $geo_zone['geo_zone_id']; ?>"><?= $geo_zone['name']; ?></option>
                <?php } ?>
                <?php } ?>
              </select></td>
          </tr>
          <tr>
            <td><?= $entry_status; ?></td>
            <td><select name="bank_transfer_status">
                <?php if ($bank_transfer_status) { ?>
                <option value="1" selected="selected"><?= $text_enabled; ?></option>
                <option value="0"><?= $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?= $text_enabled; ?></option>
                <option value="0" selected="selected"><?= $text_disabled; ?></option>
                <?php } ?>
              </select></td>
          </tr>
          <tr>
            <td><?= $entry_sort_order; ?></td>
            <td><input type="text" name="bank_transfer_sort_order" value="<?= $bank_transfer_sort_order; ?>" size="1" /></td>
          </tr>
        </table>
        </div><!-- End: #tab-general -->
        </form><!-- End: form parent(#tab-general) -->
              
        <!-- <form action="<?= $action; ?>" method="post" enctype="multipart/form-data" id="form-inactive" class="ftabs">      
         <input type="hidden" name="form_category" value="fdata" />   
         <div id="tab-data">
            <table class="form">
                <tr><td colspan="3"><b><?= $head_bank='Bank'; ?></b></td></tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;<?= $entry_bank_name='Nama'; ?><span class="required">*</span></td>
                    <td><input type="text" name="bank_master_name" value="<?= $bank_master_name; ?>" size="25" required="required"/></td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;<?= $entry_bank_division='Unit Cabang'; ?></td>
                    <td><input type="text" name="bank_master_division" value="<?= $bank_master_division; ?>" size="25" /></td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;<?= $entry_bank_address='Alamat'; ?></td>
                    <td><textarea name="bank_master_address" cols="80" rows="8"><?= $bank_master_address; ?></textarea></td>
                </tr>
                <tr><td colspan="3"><b><?= $head_customer='Nasabah'; ?></b></td></tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;<?= $entry_bank_accountnumber='Rekening'; ?><span class="required">*</span></td>
                    <td><input type="text" name="bank_master_accountnumber" value="<?= $bank_master_accountnumber; ?>" size="25" required="required"/></td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;<?= $entry_bank_accountname='Nama'; ?><span class="required">*</span></td>
                    <td><input type="text" name="bank_master_accountname" value="<?= $bank_master_accountname; ?>" size="55" required="required"/></td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;<?= $entry_bank_accountaddress='Alamat'; ?><span class="required">*</span></td>
                    <td><textarea name="bank_master_accountaddress" cols="80" rows="8"><?= $bank_master_accountaddress; ?></textarea></td>
                </tr>
                <tr>
                    <td><?= $entry_sort_order; ?></td>
                    <td><input type="text" name="bank_master_sequence" value="<?= $bank_master_sequence; ?>" size="1" /></td>
                </tr>
            </table>
         </div><!-- End: #tab-data --     
        </form><!-- End: form parent(#tab-data) --      -->
    </div>
  </div>
</div>

<script type="text/javascript">
    $('#tabs a').tabs(); 
    
    $(function(){
       'use strict'; 
       
        $('#tabs a').on('click', function(){
          $('form.ftabs').attr('id','form-inactive'); // jadikan semua id form inactive
          var $id_tab = $(this).attr('href'); 
          $($id_tab).parents('form').attr('id','form'); // jadikan form active pada tab yg aktif
        });
    })
</script>              
              
<?= $footer; ?>