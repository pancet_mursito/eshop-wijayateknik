<?php 
require_once dirname(dirname(__FILE__)) . '/__subcontroller/uncleController.php';

class ControllerPaymentBankTransfer extends UncleController 
{
        public static $form_category;
        private $error = array(); 

	public function index() {
		$this->language->load('payment/bank_transfer');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');
			
		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
                    self::$form_category = array_shift($this->request->post);
                    if ($this->validate()) {
                        switch (self::$form_category) {
                            case 'fgeneral':
                                $this->model_setting_setting->editSetting('bank_transfer', $this->request->post);
                                $this->session->data['success'] = $this->language->get('text_success');
                                $this->redirect($this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL')); 
                                break;
                            case 'fdata':
                                return null; // inprogress..
                                break;
                        }
                    }
		}

		$this->data['heading_title']        = $this->language->get('heading_title');

		$this->data['text_enabled']         = $this->language->get('text_enabled');
		$this->data['text_disabled']        = $this->language->get('text_disabled');
		$this->data['text_all_zones']       = $this->language->get('text_all_zones');
		
		$this->data['entry_bank']           = $this->language->get('entry_bank');
		$this->data['entry_total']          = $this->language->get('entry_total');	
		$this->data['entry_order_status']   = $this->language->get('entry_order_status');		
		$this->data['entry_geo_zone']       = $this->language->get('entry_geo_zone');
		$this->data['entry_status']         = $this->language->get('entry_status');
		$this->data['entry_sort_order']     = $this->language->get('entry_sort_order');
		
		$this->data['button_save']          = $this->language->get('button_save');
		$this->data['button_cancel']        = $this->language->get('button_cancel');
                
                #[additional]
                $this->data['tab_general']          = $this->language->get('tab_general');
                $this->data['tab_data']             = $this->language->get('tab_data');
                #[end]

 		if (isset($this->error['warning'])) {
                       $this->data['error_warning'] = $this->error['warning'];
		} else {
                       $this->data['error_warning'] = '';
		}
		
		$this->load->model('localisation/language');
		
		$languages = $this->model_localisation_language->getLanguages();
		
		foreach ($languages as $language) {
                    if (isset($this->error['bank_' . $language['language_id']])) {
                      $this->data['error_bank_' . $language['language_id']] = $this->error['bank_' . $language['language_id']];
                    } else {
                      $this->data['error_bank_' . $language['language_id']] = '';
                    }
		}
		
  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
                        'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
                        'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
                        'text'      => $this->language->get('text_payment'),
			'href'      => $this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL'),
                        'separator' => ' :: '
   		);

   		$this->data['breadcrumbs'][] = array(
                        'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('payment/bank_transfer', 'token=' . $this->session->data['token'], 'SSL'),
                        'separator' => ' :: '
   		);
		
                #[create links form]
		$this->data['action'] = $this->url->link('payment/bank_transfer', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['cancel'] = $this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL');
                
                #[load model]
		$this->load->model('localisation/language');
		
                #[assigment & controlling] 
                if(self::$form_category == 'fgeneral' || !isset(self::$form_category)){
		   foreach ($languages as $language) {
                     $lang_id = $language['language_id'];
                     $this->data['bank_transfer_bank_'.$lang_id] = $this->inputControl('bank_transfer_bank_'.$lang_id);
		   }
                }
		$this->data['languages'] = $languages;
                
                $this->data['bank_transfer_total'] = $this->inputControl('bank_transfer_total');
                $this->data['bank_transfer_order_status_id'] = $this->inputControl('bank_transfer_order_status_id');
                
                #[[additional]]
                $this->data['bank_master_name']           = $this->inputControl('bank_master_name');
                $this->data['bank_master_division']       = $this->inputControl('bank_master_division');
                $this->data['bank_master_address']        = $this->inputControl('bank_master_address');
                $this->data['bank_master_sequence']       = $this->inputControl('bank_master_sequence');
                $this->data['bank_master_accountname']    = $this->inputControl('bank_master_accountname');
                $this->data['bank_master_accountnumber']  = $this->inputControl('bank_master_accountnumber');
                $this->data['bank_master_accountaddress'] = $this->inputControl('bank_master_accountaddress');
                
                
		$this->load->model('localisation/order_status');
		
		$this->data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();
		
                $this->data['bank_transfer_geo_zone_id'] = $this->inputControl('bank_transfer_geo_zone_id');
                
		$this->load->model('localisation/geo_zone');
										
		$this->data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();
		
                $this->data['bank_transfer_status'] = $this->inputControl('bank_transfer_status');
		$this->data['bank_transfer_sort_order'] = $this->inputControl('bank_transfer_sort_order');

		$this->template = 'payment/bank_transfer.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
		#[rendering view]		
		$this->response->setOutput($this->render());
	}

	protected function validate() 
        {
                #[checking permission]
		if (!$this->user->hasPermission('modify', 'payment/bank_transfer')) {
		     $this->error['warning'] = $this->language->get('error_permission');
		}
                #[loading model]    
		$this->load->model('localisation/language');
                #[assigning method]
		$languages = $this->model_localisation_language->getLanguages();
		#[daftarkan (tag name) yang akan di validasi pada tab masing-masing]
                switch (self::$form_category) {
                    case 'fgeneral':
                        foreach ($languages as $language) {
                          if (!$this->request->post['bank_transfer_bank_' . $language['language_id']]) {
                               $this->error['bank_' .  $language['language_id']] = $this->language->get('error_bank');
                          }
                        }
                        break;
                    case 'fdata':
                        return null; // inprogress..
                        break;
                }
                return (!$this->error) ? TRUE : FALSE;
	}
}
?>