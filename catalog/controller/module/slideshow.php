<?php  
class ControllerModuleSlideshow extends Controller 
{
	protected function index($setting) 
        {
		static $module = 0;
		
		$this->load->model('design/banner');
		$this->load->model('tool/image');
		
		$this->document->addScript('catalog/view/javascript/jquery/nivo-slider/jquery.nivo.slider.pack.js');
		
		if (file_exists('catalog/view/theme/' . $this->config->get('config_template') . '/stylesheet/slideshow.css')) {
                    $this->document->addStyle('catalog/view/theme/' . $this->config->get('config_template') . '/stylesheet/slideshow.css');
		} else {
                    $this->document->addStyle('catalog/view/theme/default/stylesheet/slideshow.css');
		}
		
		$this->data['width']   = $setting['width'];
		$this->data['height']  = $setting['height'];
		$this->data['banners'] = array();
		
		if (isset($setting['banner_id'])) {
			$results = $this->model_design_banner->getBanner($setting['banner_id']);
			  
			foreach ($results as $result) {
				if (file_exists(DIR_IMAGE . $result['image'])) {
					$this->data['banners'][] = array(
						'title' => $result['title'],
						'link'  => $result['link'],
						'image' => $this->model_tool_image->resize($result['image'], $setting['width'], $setting['height'])
					);
				}
			}
		}
		
		$this->data['module'] = $module++;
                
                #--/ greenhole writed [[
                # Categories
                $this->data['categories']       = $this->getCategories();
                
                # Detail Company
                $this->data['company_profile']  = $this->getDetailCorp(4);
                $this->data['details_corp']     = $this->getDetailCorp();
                #--/ End ]]
						
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/slideshow.tpl')) {
                    $this->template = $this->config->get('config_template') . '/template/module/slideshow.tpl';
		} else {
                    $this->template = 'default/template/module/slideshow.tpl';
		}
		$this->render();
	}
        
        protected function getDetailCorp($information_id = '')
        {
            $this->load->model('catalog/information');
            
            if (!empty($information_id)) {
                return $this->model_catalog_information->getInformation($information_id);
            } else {
                return $this->model_catalog_information->getInformations();
            }
        }
        
        protected function getCategories()
        {
            // Menu
            $this->load->model('catalog/category');
            $this->load->model('catalog/product');

            $this->data['categories'] = array();

            $categories = $this->model_catalog_category->getCategories(0);

            foreach ($categories as $category) {
               if($category['top']) {
                     // Level 2
                     $children_data = array();

                     $children = $this->model_catalog_category->getCategories($category['category_id']);

                     foreach ($children as $child) {
                        $data = array(
                            'filter_category_id'  => $child['category_id'],
                            'filter_sub_category' => true
                        );

                        $product_total = $this->model_catalog_product->getTotalProducts($data);

                        $children_data[] = array(
                            'name'  => $child['name'] . ($this->config->get('config_product_count') ? ' (' . $product_total . ')' : ''),
                            'href'  => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'])
                        );						
                     }	
                     // Level 1
                     $this->data['categories'][] = array(
                            'name'     => $category['name'],
                            'children' => $children_data,
                            'column'   => $category['column'] ? $category['column'] : 1,
                            'href'     => $this->url->link('product/category', 'path=' . $category['category_id'])
                     );
                }
            }
            return $categories;
        }
        
} #--/ End: Class

