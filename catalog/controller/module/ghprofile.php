<?php
/**
 * @title : [] 
 * @author: greenhole.web.id <mursito@greenhole.web.id>
 * @date  : []
 */

class ControllerModuleGhprofile extends Controller
{
    protected function index()
    {
        #--/ loading language integration.
        $this->language->load('common/gh_additional');
        
        #--/ assign variable to view.
        $this->data['heading_title'] = $this->language->get('heading_title');
        $this->data['foo'] = __CLASS__;
        
        #--/ Periksa view template & tentukan template-nya.
        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/gh_profile.tpl')) {
            $this->template = $this->config->get('config_template') . '/template/common/gh_profile.tpl';
	} else {
            $this->template = 'default/template/common/gh_profile.tpl';
	}
        
        #--/ Rendering view.
        $this->render();
    }
    
} #--/ End: Class