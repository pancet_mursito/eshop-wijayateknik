<?php

class ControllerModuleCategory extends Controller 
{
    public $gh_headTitleCategories = TRUE;
        
	protected function index($setting)
        {
		$this->language->load('module/category');
		
        $this->data['heading_title'] = $this->language->get('heading_title');
		
        // Inserting Carousel Module
        $this->document->addScript('catalog/view/javascript/jquery/jquery.jcarousel.min.js');

        if (file_exists('catalog/view/theme/' . $this->config->get('config_template') . '/stylesheet/carousel.css')) {
            $this->document->addStyle('catalog/view/theme/' . $this->config->get('config_template') . '/stylesheet/carousel.css');
        } else {
            $this->document->addStyle('catalog/view/theme/default/stylesheet/carousel.css');
        }

        // Desc...
		if (isset($this->request->get['path'])) {
            $parts = explode('_', (string)$this->request->get['path']);
		} else {
            $parts = array();
		}
		
		if (isset($parts[0])) {
            $this->data['category_id'] = $parts[0];
		} else {
            $this->data['category_id'] = 0;
		}
		
		if (isset($parts[1])) {
            $this->data['child_id'] = $parts[1];
		} else {
            $this->data['child_id'] = 0;
		}
		
        // Desc...					
		$this->load->model('catalog/category');
		$this->load->model('catalog/product');

		//\default script >> $this->data['categories'] = array();
                
        $collect_categories = array();
                
		$categories = $this->model_catalog_category->getCategories();
                
		foreach($categories as $category) {
			$total = $this->model_catalog_product->getTotalProducts(array('filter_category_id' => $category['category_id']));

			$children_data = array();

			$children = $this->model_catalog_category->getCategories($category['category_id']);

			foreach($children as $child) {
				$data = array(
					'filter_category_id'  => $child['category_id'],
					'filter_sub_category' => true
				);

				$product_total = $this->model_catalog_product->getTotalProducts($data);

				$total += $product_total;

				$children_data[] = array(
					'category_id' => $child['category_id'],
                    'layout_id'   => $this->model_catalog_category->getCategoryLayoutId($category['category_id']),
					'name'        => $child['name'] . ($this->config->get('config_product_count') ? ' (' . $product_total . ')' : ''),
					'href'        => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id']),	
				);		
			}

			$collect_categories[] = array(
				    'category_id' => $category['category_id'],
                    'layout_id'   => $this->model_catalog_category->getCategoryLayoutId($category['category_id']),
                    'name'        => $category['name'] . ($this->config->get('config_product_count') ? ' (' . $total . ')' : ''),
                    'href'        => $this->url->link('product/category', 'path=' . $category['category_id']),
                    'sort_order'  => $category['sort_order'],
                    'image'       => $category['image'],
                    'description' => $category['description'], 
                    'animation'   => $this->model_catalog_category->getAnimationCategory($category['gh_animation_id']),
                    'children'    => $children_data,
			);	
                        
		}

        // Membangun ulang struktur array dari `categories`
        $this->gh_headTitleCategories = FALSE;
        $categories_animation  = $this->rebuild_forAnimation($collect_categories);
        $categories_animation2 = array_chunk($collect_categories, 4);
                
        // Assignment To View
        $this->data['title_status']          = $this->gh_headTitleCategories;
        $this->data['categories']            = $collect_categories;
        $this->data['categories_animation']  = $categories_animation;
        $this->data['categories_animation2'] = $categories_animation2;
        $this->data['param_setting']         = $setting;
                
        // Pagination -- Belum Dipakai!!
        $pagination = new Pagination();
        $pagination->total = count($categories_animation);
        $pagination->limit = 2;
        $this->data['pagination'] = $pagination->render();
                
        // Gunakan view bawaan jika tidak ada di template eksternal        
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/category.tpl')) {
            $this->template = $this->config->get('config_template') . '/template/module/category.tpl';
		} else {
            $this->template = 'default/template/module/category.tpl';
		}
		
        // Eksekusi ke view
		$this->render();
  	} 
        
    public function get_categoriesChildren($setting=array())
    {
            //[[ Loader
            $this->language->load('module/category');
            $this->language->load('module/featured'); 
            
            $this->load->model('catalog/product');
            $this->load->model('tool/image');
            
            //[[ Assignment
            $parent_id = $this->request->post['parent_id'];
            $products  = $this->model_catalog_product->getProductsByCategory($parent_id);
            
            //[[ Assign data to view 
            $this->data['heading_title']      = $this->language->get('heading_title');
            $this->data['button_cart']        = $this->language->get('button_cart');
            $this->data['button_addwishlist'] = $this->language->get('button_addwishlist');
            $this->data['button_addcompare']  = $this->language->get('button_addcompare');
            
            if (empty($setting['image_width']))  $setting['image_width']  = 100;
            if (empty($setting['image_height'])) $setting['image_height'] = 100;
            
            //[[ Penyusunan ulang struktur products
            $ls_products = array();
            foreach($products as $product_info)
            {
               if ($product_info) {
                   if ($product_info['image']) {
                       $image = $this->model_tool_image->resize($product_info['image'], $setting['image_width'], $setting['image_height']);
                   } else {
                       $image = false;
                   }
                   if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                       $price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')));
                   } else {
                       $price = false;
                   }
                   if ((float)$product_info['special']) {
                       $special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')));
                   } else {
                       $special = false;
                   }
                   if ($this->config->get('config_review_status')) {
                       $rating = $product_info['rating'];
                   } else {
                       $rating = false;
                   }
                   
                   $ls_products[] = array(
                       'product_id' => $product_info['product_id'],
                       'thumb'      => $image,
                       'name'       => $product_info['name'],
                       'price'      => $price,
                       'special'    => $special,
                       'rating'     => $rating,
                       'reviews'    => sprintf($this->language->get('text_reviews'), (int)$product_info['reviews']),
                       'href'       => $this->url->link('product/product', 'product_id=' . $product_info['product_id']),
                   );
               }    
            }
            $this->data['products'] = $ls_products;
            
            //[[ Periksa path dari view
            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/gh_product.tpl')) {
                $this->template = $this->config->get('config_template') . '/template/module/gh_product.tpl';
            } else {
                $this->template = 'default/template/module/gh_product.tpl';
            }
            
            //[[ Output
            $this->response->setOutput($this->render());
            
            /***
            /*** Pengambilan data dengan cara aslinya !?
            $this->data['featured_product']   = $this->config->get('featured_product');
            $_products = explode(',', $this->config->get('featured_product'));
            $_products = array_slice($_products, 0, (int)$setting['limit']);
            if (empty($setting['limit'])) $setting['limit'] = 7;
            ***
            foreach($_products as $product_id) 
            {
        			$product_info = $this->model_catalog_product->getProduct($product_id);
        			
        			if ($product_info) {
        				if ($product_info['image']) {
                            $image = $this->model_tool_image->resize($product_info['image'], $setting['image_width'], $setting['image_height']);
        				} else {
                            $image = false;
        				}

        				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                            $price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')));
        				} else {
                            $price = false;
        				}
        						
        				if ((float)$product_info['special']) {
                            $special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')));
        				} else {
                            $special = false;
        				}
        				
        				if ($this->config->get('config_review_status')) {
                            $rating = $product_info['rating'];
        				} else {
                            $rating = false;
        				}
        					
        				$this->data['produk'][] = array(
        					'product_id' => $product_info['product_id'],
        					'thumb'      => $image,
        					'name'       => $product_info['name'],
        					'price'      => $price,
        					'special'    => $special,
        					'rating'     => $rating,
        					'reviews'    => sprintf($this->language->get('text_reviews'), (int)$product_info['reviews']),
        					'href'       => $this->url->link('product/product', 'product_id=' . $product_info['product_id'])
        				);
        			}
            }
            //$this->data['produk'] = $_products;

            $this->render()
            //\ End !? ****/
        }         
        
        protected function rebuild_forAnimation($arrays,$n_column=3)
        {
            if ($this->gh_headTitleCategories) {
                $adds_front = array (
                            'category_id'  => '',
                            'layout_id'    => '1',
                            'name'         => $this->language->get('heading_title'),
                            'href'         => '',
                            'sort_order'   => '0',
                            'image'        => '',
                            'description'  => '',
                            'animation'    => array (
                                            'id'         => '0',
                                            'classname'  => 'chameleon_1',
                                            'label'      => 'Chameleon One',
                                            'size_class' => 'tile-big',
                                            'color_class'=> 'tile-5',
                                            'html_tag'   => '<div><p></p></div>',
                            ),
                            'children'     => array (),
                );
                array_unshift($arrays, $adds_front);
            }    
            
            $m = ceil(count($arrays) / $n_column);
            $y = array_chunk($arrays,$m);
            
            return $y;
        }        

        protected function rebuild_forAnimation_mode_1($array_animation) #--/ TIDAK DIGUNAKAN (OLD WAY) !!
        {
            #--/ pecah array menjadi susunan 3 kolom.
            $sources = array_chunk($array_animation, 3);
            
            #--/ cari jumlah rata-rata per-kolom.
            $collect = array();
            foreach ($sources as $elm) {
               $collect[] = count($elm); 
            }
            
            $mean_eachColumn = ceil( array_sum($collect) / count($sources) );
            
            #--/ ambil 1 elemen dari kolom 1 & taruh ke kolom yg paling sedikit.
            $new_build = array();
            
            foreach($sources as $ii => $elm) {
                $sisa = array();
				
             	if ($ii == 0 && count($elm) == $mean_eachColumn) {
                    $temp   = array_pop($elm);
                    $sisa[] = $temp;
                }
                if (count($elm) < $mean_eachColumn) {
                    $elm = array_merge($elm, $sisa);
                }
                $new_build[] = $elm;
            }
            
            #--/ buang elemen terakhir dari kolom 1.
            foreach($new_build as $ii => &$elm) {
                if ($ii == 0) {
                    $trash = array_pop($elm);
                }
            }
            return $new_build;
        }
}
