<?php
// Heading 
$_['heading_title']      = 'Products';

// Button
$_['button_addwishlist'] = 'Add to Wish List';
$_['button_addcompare']  = 'Add to Compare';

// Text
$_['text_reviews']       = 'Based on %s reviews.'; 
?>