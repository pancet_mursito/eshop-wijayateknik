<?php
// Text
$_['text_home']           = 'Beranda';
$_['text_wishlist']       = 'Daftar Pemesanan (%s)';
$_['text_shopping_cart']  = 'Keranjang Belanja';
$_['text_search']         = 'Cari';
$_['text_welcome']        = 'Selamat datang, anda dapat <a href="%s">login</a> atau <a href="%s">buat akun</a>.';
$_['text_logged']         = 'Anda login sebagai <a href="%s">%s</a> <b>(</b> <a href="%s">Keluar</a> <b>)</b>';
$_['text_account']        = 'Akun saya';
$_['text_checkout']       = 'Periksa';
?>