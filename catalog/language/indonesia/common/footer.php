<?php
// Text
$_['text_information']  = 'Informasi';
$_['text_service']      = 'Layanan Pengguna';
$_['text_extra']        = 'Ekstra';
$_['text_contact']      = 'Kontak Kami';
$_['text_return']       = 'Kembali';
$_['text_sitemap']      = 'Peta Situs';
$_['text_manufacturer'] = 'Merek';
$_['text_voucher']      = 'Voucher Hadiah';
$_['text_affiliate']    = 'Afiliasi';
$_['text_special']      = 'Spesial';
$_['text_account']      = 'Akun Saya';
$_['text_order']        = 'Sejarah Pemesanan';
$_['text_wishlist']     = 'Daftar Pemesanan';
$_['text_newsletter']   = 'Newsletter';
$_['text_powered']      = 'Dikembangkan oleh <a href="http://greenhole.web.id">GreenHole</a> &bull; <a href="http://www.opencart.com">OpenCart</a><br /> %s &copy; %s';
?>