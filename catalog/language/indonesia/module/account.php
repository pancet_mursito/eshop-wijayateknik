<?php
// Heading 
$_['heading_title']    = 'Akun';

// Text
$_['text_register']    = 'Registrasi';
$_['text_login']       = 'Login';
$_['text_logout']      = 'Keluar';
$_['text_forgotten']   = 'Lupa Sandi';
$_['text_account']     = 'Akun Saya';
$_['text_edit']        = 'Edit Akun';
$_['text_password']    = 'Sandi';
$_['text_address']     = 'Buku Alamat';
$_['text_wishlist']    = 'Daftar Pemesanan';
$_['text_order']       = 'Sejarah Pemesanan';
$_['text_download']    = 'Unduhan';
$_['text_return']      = 'Kembali';
$_['text_transaction'] = 'Transaksi';
$_['text_newsletter']  = 'Newsletter';
?>
