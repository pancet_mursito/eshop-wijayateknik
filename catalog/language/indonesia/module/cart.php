<?php
// Heading 
$_['heading_title'] = 'Keranjang Belanja';

// Text
$_['text_items']    = '%s item(s) - %s';
$_['text_empty']    = 'Keranjang belanja anda kosong!';
$_['text_cart']     = 'Tampilkan Keranjang';
$_['text_checkout'] = 'Pemeriksaan';
?>