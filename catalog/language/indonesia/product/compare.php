<?php

// Heading
$_['heading_title']     = 'Perbandingan Produk';
 
// Text
$_['text_product']      = 'Detil Produk';
$_['text_name']         = 'Produk';
$_['text_image']        = 'Gambar';
$_['text_price']        = 'Harga';
$_['text_model']        = 'Model';
$_['text_manufacturer'] = 'Merek';
$_['text_availability'] = 'Ketersediaan';
$_['text_instock']      = 'Stok';
$_['text_rating']       = 'Penilaian';
$_['text_reviews']      = 'Berdasarkan ulasan %s.';
$_['text_summary']      = 'Ringkasan';
$_['text_weight']       = 'Berat';
$_['text_dimension']    = 'Dimensi (P x L x H)';
$_['text_compare']      = 'Produk Bandingkan (% s)';
$_['text_success']      = 'Sukses: Anda telah menambahkan <a target="_blank" href="%s">%s</a> ke <a target="_blank" href="%s">perbandingan produk anda</a>!';
$_['text_remove']       = 'Sukses: Anda telah memodifikasi produk perbandingan Anda';
$_['text_empty']        = 'Anda belum memilih produk-produk untuk membandingkan.';

#--/ Close PHP ?>