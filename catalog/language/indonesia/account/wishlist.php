<?php
/*
// Heading 
$_['heading_title'] = 'My Wish List';

// Text
$_['text_account']  = 'Account';
$_['text_instock']  = 'In Stock';
$_['text_wishlist'] = 'Wish List (%s)';
$_['text_login']    = 'You must <a href="%s">login</a> or <a href="%s">create an account</a> to save <a href="%s">%s</a> to your <a href="%s">wish list</a>!';
$_['text_success']  = 'Success: You have added <a href="%s">%s</a> to your <a href="%s">wish list</a>!';
$_['text_remove']   = 'Success: You have modified your wishlist!';
$_['text_empty']    = 'Your wish list is empty.';

// Column
$_['column_image']  = 'Image';
$_['column_name']   = 'Product Name';
$_['column_model']  = 'Model';
$_['column_stock']  = 'Stock';
$_['column_price']  = 'Unit Price';
$_['column_action'] = 'Action';
*/

// Heading 
$_['heading_title'] = 'Daftar Pemesanan Saya';

// Teks
$_['text_account']  = 'Akun';
$_['text_instock']  = 'Stok';
$_['text_wishlist'] = 'Daftar Pemesanan (% s)';
$_['text_login']    = 'Anda harus <a href="%s"> masuk </ a> atau href="%s"> <a membuat akun </ a> untuk menyimpan <a href = "% s ">% s </ a> untuk href="%s"> <a daftar keinginan </ a> !';
$_['text_success']  = 'Sukses: Anda telah menambahkan <a href="%s">% s </ a> untuk href="%s"> <a daftar keinginan </ a>';
$_['text_remove']   = 'Sukses: Anda telah memodifikasi daftar pemesanan Anda';
$_['text_empty']    = 'daftar keinginan Anda kosong.';

// Kolom
$_['column_image']  = 'Gambar';
$_['nama_kolom']    = 'Nama Produk';
$_['column_model']  = 'Model';
$_['column_stock']  = 'Saham';
$_['column_price']  = 'Harga Satuan';
$_['column_action'] = 'Aksi';
?>