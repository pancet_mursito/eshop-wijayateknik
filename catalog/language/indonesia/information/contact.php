<?php
// Heading
$_['heading_title']  = 'Kontak Kami';

// Text 
$_['text_location']  = 'Lokasi Kami';
$_['text_contact']   = 'Formulir Kontak';
$_['text_address']   = 'Alamat:';
$_['text_email']     = 'Surel:';
$_['text_telephone'] = 'Telepon:';
$_['text_fax']       = 'Fax:';
$_['text_message']   = '<p>Permintaan Anda telah berhasil dikirim ke pemilik toko!</p>';

// Entry Fields
$_['entry_name']     = 'Nama Depan:';
$_['entry_email']    = 'Alamat Surel:';
$_['entry_enquiry']  = 'Permintaan:';
$_['entry_captcha']  = 'Masukkan kode pada kotak di bawah:';

// Email
$_['email_subject']  = 'Permintaan %s';

// Errors
$_['error_name']     = 'Nama harus antara 3 sampai 32 karakter!';
$_['error_email']    = 'Alamat surel tampaknya tidak valid!';
$_['error_enquiry']  = 'Permintaan harus antara 10 sampai 3000 karakter!';
$_['error_captcha']  = 'Kode verifikasi tidak sesuai gambar!';
?>
