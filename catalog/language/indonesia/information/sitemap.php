<?php
// Heading
$_['heading_title']    = 'Peta Situs';
 
// Text
$_['text_special']     = 'Penawaran Spesial';
$_['text_account']     = 'Akun Saya';
$_['text_edit']        = 'Informasi Akun';
$_['text_password']    = 'Sandi';
$_['text_address']     = 'Buku Alamat';
$_['text_history']     = 'Sejarah Pemesanan';
$_['text_download']    = 'Unduhan';
$_['text_cart']        = 'Keranjang Belanja';
$_['text_checkout']    = 'Pemeriksaan';
$_['text_search']      = 'Cari';
$_['text_information'] = 'Informasi';
$_['text_contact']     = 'Kontak Kami';
?>
