/* 
 * @copyright: http://www.zopim.com
 * @implementator: http://www.greenhole.web.id <mursito@greenhole.web.id>
 * @title: Live Chat Feature
 * @license: GNU Public
 */

window.$zopim||(function(d,s){
    var z=$zopim=function(c){z._.push(c)},$=z.s=d.createElement(s),e=d.getElementsByTagName(s)[0];
    z.set=function(o){
        z.set._.push(o)};
        z._=[];
        z.set._=[];
        $.async=!0;
        $.setAttribute('charset','utf-8');
        $.src='//v2.zopim.com/?1j4cpPfeNOoh7P1CkZmg1jchQOwQLTKl';
        z.t=+new Date;
        $.type='text/javascript';
        e.parentNode.insertBefore($,e)
})(document,'script');


