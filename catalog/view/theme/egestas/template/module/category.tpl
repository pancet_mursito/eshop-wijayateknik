<?php if ( !isset($this->request->get['route']) || $this->request->get['route'] == 'common/home' ) {  ?>

<style type="text/css">
  .gh-form-slide {
    width: 75em; overflow: hidden; margin: 0.3em auto;
  }

  .gh-sub-form {
    width: 99999em;
  }

  .gh-slide {
    width: 75em; float: left; position: relative;
  }

  .gh-extend-jcarousel .gh-block-category {
    padding: 10px 10px 15px 10px;
  }

  .btn-category:hover {
    cursor: pointer;
  }

  .lbl-loading {
    z-index:9999;
    opacity:0.3;
    text-align:center;
    position: absolute;
    margin: 0 -1%;
  }

  .gh-extend-jcarousel .gh-block-category .gh-group-split-category {
    background: #333333;
    height: 100%;
    width: 100%;
    color: #FFFFFF;
    text-align:center;
  }

  .gh-extend-jcarousel .gh-block-category .category-item-wrapper {
    display: inline-table;
    height: 45%;
    width: 20%;
    margin: 5% auto;
  }

  .gh-extend-jcarousel .gh-block-category .category-page {
    border: 4px solid #99CC66; 
    border-radius: 50%;
    background: #DEDEDE; 
    padding: 1px;
    position: static;
    bottom: 0;
    left: 0;
    width: 2.5%;
    margin: 0 auto;
    font-weight: bold;
  }

  .gh-extend-jcarousel .gh-block-category .category-page label {
    margin: 0 auto;
    color: #333333;
    width: 2%;
    height: 4%;
  }

  .gh-extend-jcarousel .gh-block-category .category-item {
    border: 4px solid #DEDEDE; 
    border-radius: 50%;
    padding: 5px; 
  }

  .gh-extend-jcarousel .gh-block-category .category-item:hover {
    border: 4px solid #99CC66;
  }

  .gh-extend-jcarousel .gh-block-category .category-item img {
    border-radius: 50%;
    margin: 0 auto;
    background: #FFFFFF;
  }
</style>

<!--/ METRO STYLE /-->
<div id="categories-wrapper">

 <div class="box">
 <!--?= $this->request->get['route'];?-->

  <!--/ Begin: button jumping to products of category /-->
  <a id="btn-jump-category" href="#dest-jump-products" class="hide">&nbsp;</a>
  <!--/ End: button jumping to products of category /-->

  <?php if (!$title_status) { ?>
  <div class="box-heading btn-category btn-heading" data-category-id>
    <span><?php echo $heading_title; ?></span>
  </div>
  <?php } else { ?>
  <hr>
  <?php } ?>

  <div class="box-content gh-content-metro">
      
      <div class="linear metro-wrapper">
              <!--
              <div class="dashboard clearfix">
                   <ul class="tiles"><!--/ Begin: categories /--

                        <?php foreach($categories_animation as $ii => $elm): ?>

                        <div class="col<?= $ii+1;?> clearfix">

                            <?php foreach($elm as $jj => $sub_elm): ?>

                            <li class="btn-category tile <?= $sub_elm['animation']['classname']?> <?= $sub_elm['animation']['size_class']?> <?= $sub_elm['animation']['color_class']?>"
                                data-page-type="r-page"
                                data-page-name="random-r-page"
                                data-category-id="<?= $sub_elm['category_id'];?>"
                                data-name ="<?= $sub_elm['name'];?>"
                                data-image="<?= $sub_elm['image'];?>"
                                data-href ="<?= $sub_elm['href'];?>"
                                data-desc ="<?= $sub_elm['description'];?>"
                                data-animation="<?= $sub_elm['animation']['classname'];?>"
                                data-sort-order="<?= $sub_elm['sort_order'];?>"
                                data-sequence="<?= $ii;?>">
                                <?= htmlspecialchars_decode($sub_elm['animation']['html_tag']);?>
                            </li>

                            <?php endforeach;?>

                        </div><!--/ End: col /--

                        <?php endforeach;?>

                  </ul><!--/ End: categories  /--

              </div><!--/ End: .dashboard /-->
      </div><!--/ End: .metro-wrapper /-->
    </div><!--/ End: .box-content /-->
  </div><!--/ End: .box /-->
</div><!--/ End: #categories-wrapper /-->


<div class="gh-form-slide">
  <!-- <div class="gh-sub-form"></div> -->
</div>

<!--/ Carousel [for categories module] /-->
<div id="carousel_for_categories" class="gh-extend-jcarousel">
  <div class="gh-extend-jcarousel-inner">
    <ul class="gh-extend-jcarousel-skin-opencart">

      <?php foreach ($categories_animation2 as $sequence => $cgroup): ?>
      <li>
        <div id="category_group<?= $sequence;?>" class="gh-block-category">
          <div id="group-split<?= $sequence;?>" class="gh-group-split-category">

               <?php foreach ($cgroup as $jj => $category){ ?>
                <div class="category-item-wrapper">
                  <div class="category-item btn-category" 
                       id="<?= $category['category_id'];?>"
                       data-category-id="<?= $category['category_id'];?>">
                    <img src="image/<?= $category['image'];?>" height="200" width="200" />
                  </div> 
                  <div class="category-desc"><?= $category['name'];?></div>  
                </div>  
               <?php } ?>

               <div class="category-page">
                  <label><?= $sequence+1;?></label>
               </div>
          </div>

        </div>
      </li>
      <?php endforeach;?>
    </ul>

  </div>
</div>
<script type="text/javascript"><!--
$('#carousel_for_categories ul').jcarousel({
  vertical: false,
  visible: 1,
  scroll: 1,
  itemFallbackDimension: 90
});
//--></script>

<!--
<pre>< ?= print_r($categories_animation2);?></pre>
<pre>< ?= print_r($param_setting);?></pre>
<pre>< ?= print_r($categories_animation);?></pre> 
-->

<div id="products-wrapper">
    <!--/ load place product by category /-->
</div><!--/ End: .product-wrapper /-->
<br/>


<!--/ FOR METRO STYLE /-->
<script type="text/javascript">

function goToByScroll(id){
      // Remove "link" from the ID
    id = id.replace("link", "");
      // Scroll
    $('html,body').animate({
        scrollTop: $("#"+id).offset().top},
        'slow');
}

$(function(){ // Open jQuery
 'use strict';

  $('li.btn-category').each(function(i, e){

     switch($(this).data('animation')){
        case 'slideTextUp':
            $(this).find('div:first-child p').append('<img src="image/'+$(this).data('image')+'" />');
            $(this).find('div:nth-child(2) p').html($(this).data('name'));
            break;
        case 'slideTextDown':
            $(this).find('div:first-child').append('<img src="image/'+$(this).data('image')+'" />');
            $(this).find('div:nth-child(2) p').html($(this).data('name'));
            break;
        case 'slideTextRight':
            $(this).attr('title',$(this).data('name'));
            $(this).find('div:first-child').addClass('image').append('<img src="image/'+$(this).data('image')+'" />');
            $(this).find('div:nth-child(2) p').text($(this).data('name'));
            break;
        case 'slideTextLeft':
            $(this).find('div:first-child p').append('<img src="image/'+$(this).data('image')+'" />');
            $(this).find('div:nth-child(2) p').html($(this).data('name'));
            break;
        case 'fig-tile':
            $(this).find('img').attr('src', 'image/' + $(this).data('image'));
            $(this).find('figcaption p').html($(this).data('name'));
            break;
        case 'rotate3d rotate3dY':
            $(this).find('.front span').html($(this).data('name'));
            $(this).find('.front img').attr('src', 'image/' + $(this).data('image'));
            $(this).find('.back p').html($(this).data('name'));
            break;
        case 'rotate3d rotate3dX':
            $(this).find('.front span').html($(this).data('name'));
            $(this).find('.front img').attr('src', 'image/' + $(this).data('image'));
            $(this).find('.back p').html($(this).data('name'));
            break;
        case 'chameleon_1':
            $(this).find('p').html($(this).data('name'));
            break;
        default:
            $(this).find('p').html($(this).data('name'));
            break;
     }
  });

  $('.btn-category').on('click', function(){
     if ($(this).data('category-id') == '') {
         $('#products-wrapper').children().remove();
     } else {
         $(this).append('<label class="lbl-loading">Loading...</label>'); 

         $.ajax({
           type: 'post',
           url : 'index.php?route=module/category/get_categoriesChildren',
           data: { parent_id: $(this).data('category-id') },
           success: function(rsp){
              $('#products-wrapper').html(rsp);
              $('.lbl-loading').remove();
              goToByScroll('products-wrapper');
           }
         });
     }
  });

  $('.btn-slide-wrapper a').live('click', function(){

      var nav_type = $(this).attr('name');

      switch (nav_type) {
        case 'gh-nav-next':
          //$('.metro-wrapper ul.tiles').slideUp();
          break;
        case 'gh-nav-prev':
          //$('.metro-wrapper ul.tiles').slideDown();
          break;
      }
  });

  $('a.gh-next').click(function(e){
        //$(this).closest('.gh-slide').next().animate({"left": "-=50em"}, 500);
        e.preventDefault();
        $('.gh-slide').animate({"left": "-=75em"}, 500);
  });

  $('a.gh-prev').click(function(e){
        //$(this).closest('.gh-slide').animate({"left": "+=75em"}, 500);
        e.preventDefault();
        $('.gh-slide').animate({"left": "+=75em"}, 500);
  });

}) // Close jQuery
</script>

<?php } else { ?>

<!--/ DEFAULT TAG STRUCTURE /-->
<div class="box">
  <div class="box-heading"><span><?php echo $heading_title; ?></span></div>

  <div class="box-content">
    <ul class="box-category treemenu">
      <?php foreach ($categories as $category) { ?>
      <li>
        <?php if ($category['category_id'] == $category_id) { ?>
        <a href="<?php echo $category['href']; ?>" class="active"><span><?php echo $category['name']; ?></span></a>
        <?php } else { ?>
        <a href="<?php echo $category['href']; ?>"><span><?php echo $category['name']; ?></span></a>
        <?php } ?>
        <?php if ($category['children']) { ?>
        <ul>
          <?php foreach ($category['children'] as $child) { ?>
          <li>
            <?php if ($child['category_id'] == $child_id) { ?>
            <a href="<?php echo $child['href']; ?>" class="active"><span><?php echo $child['name']; ?></span></a>
            <?php } else { ?>
            <a href="<?php echo $child['href']; ?>"><span><?php echo $child['name']; ?></span></a>
            <?php } ?>
          </li>
          <?php } ?>
        </ul>
        <?php } ?>
      </li>
      <?php } ?>
    </ul>
  </div>
</div>

<?php } ?><!--/ End: if /-->
