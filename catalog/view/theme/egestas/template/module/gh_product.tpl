<?php //[[ Open PHP
  $option = $this->config->get('featured_module');
  if($option && is_array($option)) $option = array_shift($option);
//[[ Close PHP ?>

<div class="box">
    
  <!--div class="box-heading">
      <span><?= $heading_title;?></span>
      <span class="wrapper-jumping"><a class="btn-jumping" href="#categories-wrapper">&bull;&bull;&bull;</a></span>
  </div-->
  
  <!--/ TREE MODE /-->
  <!--/
  <div class="breadcrumb">
    < ?php foreach ($breadcrumbs as $breadcrumb) { ?>
    < ?php echo $breadcrumb['separator']; ?><a href="< ?php echo $breadcrumb['href']; ?>">< ?php echo $breadcrumb['text']; ?></a>
    < ?php } ?>
  </div>
  //-->
  
  <!--/ FILTER MODE /-->
  <!--/
  <div class="product-filter clearafter hide">
        <div class="display">
            <span class="icon-list-grey">List</span>
                <a class="icon-grid" onclick="display('grid');">Grid</a>
            </div>
        <div class="product-compare">
            <a id="compare-total" class="icon-compare-grey" href="http://__freelance/app_wijayateknik/index.php?route=product/compare">Product Compare (0)</a>
        </div>
        <div class="limit">Show:
            <select onchange="location = this.value;">
                <option selected="selected" value="http://__freelance/app_wijayateknik/index.php?route=product/category&path=67&limit=15">15</option>
                <option value="http://__freelance/app_wijayateknik/index.php?route=product/category&path=67&limit=25">25</option>
                <option value="http://__freelance/app_wijayateknik/index.php?route=product/category&path=67&limit=50">50</option>
                <option value="http://__freelance/app_wijayateknik/index.php?route=product/category&path=67&limit=75">75</option>
                <option value="http://__freelance/app_wijayateknik/index.php?route=product/category&path=67&limit=100">100</option>
            </select>
        </div>
        <div class="sort">Sort By:
            <select onchange="location = this.value;">
                <option selected="selected" value="http://__freelance/app_wijayateknik/index.php?route=product/category&path=67&sort=p.sort_order&order=ASC">Default</option>
                <option value="http://__freelance/app_wijayateknik/index.php?route=product/category&path=67&sort=pd.name&order=ASC">Name (A - Z)</option>
                <option value="http://__freelance/app_wijayateknik/index.php?route=product/category&path=67&sort=pd.name&order=DESC">Name (Z - A)</option>
                <option value="http://__freelance/app_wijayateknik/index.php?route=product/category&path=67&sort=p.price&order=ASC">Price (Low > High)</option>
                <option value="http://__freelance/app_wijayateknik/index.php?route=product/category&path=67&sort=p.price&order=DESC">Price (High > Low)</option>
                <option value="http://__freelance/app_wijayateknik/index.php?route=product/category&path=67&sort=rating&order=DESC">Rating (Highest)</option>
                <option value="http://__freelance/app_wijayateknik/index.php?route=product/category&path=67&sort=rating&order=ASC">Rating (Lowest)</option>
                <option value="http://__freelance/app_wijayateknik/index.php?route=product/category&path=67&sort=p.model&order=ASC">Model (A - Z)</option>
                <option value="http://__freelance/app_wijayateknik/index.php?route=product/category&path=67&sort=p.model&order=DESC">Model (Z - A)</option>
            </select>
        </div>
  </div>
  //-->
  <div class="box-content">
    
    <div class="box-product product-grid">
      <?php foreach ($products as $product) { ?>
      <div>
        <?php if ($product['thumb']) { ?>
        <div class="image">
            <a target="_blank" href="<?= $product['href']; ?>" style="<?= ($option['image_height'] < 162) ? 'line-height: 162px' : ''; ?>">
                <img src="<?= $product['thumb']; ?>" alt="<?= $product['name']; ?>" />
            </a>
        </div>
        <?php } else { ?>
        <div class="image">
            <span class="no-image" style="<?= ($option['image_width'] < 162) ? 'width: 162px' : 'width: '.$option['image_width'].'px'; ?>; <?= ($option['image_height'] < 162) ? 'line-height: 162px' : 'line-height: '.$option['image_height'].'px;'; ?>">
            <img src="image/no_image.jpg" alt="<?= $product['name']; ?>" /></span>
	      </div>
        <?php } ?>
        <?php if ($product['price']) { ?>
        <div class="price">
          <?php if (!$product['special']) { ?>
          <div><span class="price-fixed"><?= $product['price']; ?></span></div>
          <?php } else { ?>
          <div class="special-price">
              <span class="price-old"><?= $product['price']; ?></span>
              <span class="price-fixed"><?= $product['special']; ?></span>
          </div>
          <?php } ?>
        </div>
        <?php } ?>
        <div class="name" style="width: <?= $option['image_width']; ?>px">
            <a target="_blank" href="<?= $product['href']; ?>"><?= $product['name']; ?></a>
        </div>
        <?php if ($product['rating']) { ?>
        <div class="rating">
            <img src="catalog/view/theme/egestas/image/icons/stars-<?= $product['rating']; ?>.png" alt="<?= $product['reviews']; ?>" />
        </div>
        <?php } ?>
	     <div class="details">
		    <div class="cart"><a class="button" onclick="addToCart('<?= $product['product_id']; ?>');"><span class="icon-cart-white"><?= $button_cart; ?></span></a>
        </div>
        <div class="wishlist">
            <a onclick="addToWishList('<?= $product['product_id']; ?>');">
              <span class="icon-wishlist-grey">&nbsp;<?= $button_addwishlist; ?></span>
            </a>
        </div>
        <div class="compare">
            <a onclick="addToCompare('<?= $product['product_id']; ?>');">
              <span class="icon-compare-grey">&nbsp;<?= $button_addcompare; ?></span>
            </a>
        </div>
      </div>
    </div>
    <?php } ?><!--/ End: foreach /-->
  </div><!--/ End: .box-product /-->
 </div><!--/ End: .box-content /-->
</div><!--/ End: .box /-->



