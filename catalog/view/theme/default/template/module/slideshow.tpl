<div class="slideshow">
  <div id="slideshow<?php echo $module; ?>" class="nivoSlider" style="width: <?php echo $width; ?>px; height: <?php echo $height; ?>px;">
    <?php foreach ($banners as $banner) { ?>
    <?php if ($banner['link']) { ?>
    <a href="<?php echo $banner['link']; ?>"><img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" /></a>
    <?php } else { ?>
    <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" />
    <?php } ?>
    <?php } ?>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
	$('#slideshow<?php echo $module; ?>').nivoSlider();
});
</script>

<!--
<section class="slider">
    <?php // CAMERA ?>
    <div class="camera_wrap1" id="camera_wrap_<?php echo $module; ?>">
        <?php foreach($banners as $banner){ ?>
			
	<?php if ($banner['link']) { ?>
            <div data-thumb="<?php echo $banner['image']?>" data-src="<?php echo $banner['image']?>" data-link="<?php echo $banner['link']?>">
            <?php } else { ?>
            <div data-thumb="<?php echo $banner['image']?>" data-src="<?php echo $banner['image']?>">
            <?php } ?>
                <div class="camera_caption fadeFromBottom"><?php echo $banner['title']?></div>
            </div>
        <?php } ?>
    </div>
    <?php // END CAMERA ?>
    <div class="clear"></div>
</section>

<script>
<!--
jQuery(document).ready(function(){
	
	$('#camera_wrap_<?php echo $module; ?>').camera({
		minHeight: '',
		navigation: true,
		navigationHover: true,
		pagination: true,
		thumbnails: true
		});
	
	});

</script>
-->