<?php
final class PDOs {
    public static $links;
    
    public function __construct($dbname,$dbuser,$dbpass) {
        try {
            self::$links = new PDO('mysql:host=localhost;dbname='.$dbname, $dbuser, $dbpass);
        } catch (PDOException $e) {
            print "Error!: " .$e->getMessage();
            die();
        }  
    }
}
