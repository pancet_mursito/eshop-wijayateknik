@ Asked based on application :
-------------------------------------------------------------------------

1. Tidak bisa buat kategori baru ?
   Answer : Bisa.

2. Gmn urutan waktu upload & berapa max size image-nya ?
   Answer : sudah ada di user manual yg baru.

3. Apakah link di-upload ketika mengisi related products ?
   Answer : Tidak. Isi dengan produk mana yg memiliki kesamaan.
	    Sudah ditambahkan fitur pencarian otomatis sesuai
	    dg produk yg sudah di-inputkan.

4. Apa itu menu Katalog-Atribut ?
   Answer : Merupakan menu untuk menambahkan atribut dari setiap produk. 
            Atribut yg sudah dibuat bisa digunakan untuk produk-produk 
            yg lain.

5. Apa itu menu Katalog-Opsi ?
   Answer : Tidak digunakan.
   Status : Removed.

6. Apa itu menu Katalog-Produk-[tab_profile] ?
   Answer : Tidak ada, kesalahan upload file.

7. Menu dibawah apa perlu tambahan menu pop-up u/ 
   meninggalkan pesan via email ?
   Ask : Menu dibawah yg mana ?
   Answer : ?!?


 
@ Requested list :
-------------------------------------------------------------------------

1. Warna diganti Kuning + Hitam.
   Status : Reviewed.
   Ask : warna yg sebelah mana 
	 (e.g. full-background, header, content-background, etc) ? 

2. Menu (baca: bilingual) dibuat 3 bahasa (e.g. indo, chinese, english).
   Status : Done.

3. Tulisan harga jangan menutupi produk.
   Status : Reviewed.


-------------------------------------------------------------------------

http://greenhole.web.id/			     [[ Still Learning ]]